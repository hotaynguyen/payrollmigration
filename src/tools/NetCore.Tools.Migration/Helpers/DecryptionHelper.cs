﻿using Framework.Core.StringUtils;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace NetCore.Tools.Migration.Helpers
{
    public static class DecryptionHelper
    {
        public static decimal Decode(string encodedSalary)
        {
            var result = encodedSalary.Decrypt(AppSettingsModel.SalaryEncryptKey);
            return decimal.Parse(result);
        }
        public static string Decrypt(this string value, string key)
        {
            byte[] cipherBytes = Safe64Encoding.DecodeBytes(value);
            using (var encrypt = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });

                encrypt.Key = pdb.GetBytes(32);
                encrypt.IV = pdb.GetBytes(16);

                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encrypt.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Dispose();
                    }
                    value = Encoding.ASCII.GetString(ms.ToArray());
                }
            }
            return value;
        }
    }
}
