﻿namespace NetCore.Tools.Migration
{
    public static class AppSettingsModel
    {
        public static string SalaryEncryptKey { get; set; }
        public static string PayrollSalaryEncryptKey { get; set; }
    }
}
