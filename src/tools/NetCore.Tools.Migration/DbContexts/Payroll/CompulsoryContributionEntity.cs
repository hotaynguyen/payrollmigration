﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NetCore.Tools.Migration.DbContexts.Payroll
{
    public class CompulsoryContributionEntity
    {
        public int Id { get; set; }
        public double EmployeeRate { get; set; }
        public double EmployerRate { get; set; }
        public decimal EmployeeSalaryCapInVnd { get; set; }
        public decimal EmployerSalaryCapInVnd { get; set; }
        public DateTimeOffset ApplyDate { get; set; }
        public CompulsoryContributionType CcType { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset CreatedTime { get; set; }
    }
    public enum CompulsoryContributionType
    {
        [Display(Name = "Social Insurance")]
        SocialInsurance = 1,

        [Display(Name = "Health Insurance")]
        HealthInsurance = 2,

        [Display(Name = "Unemployment Insurance")]
        UnemploymentInsurance = 3,

        [Display(Name = "Trade Union Fee")]
        TradeUnionFee = 4
    }
}
