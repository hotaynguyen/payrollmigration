﻿using Microsoft.EntityFrameworkCore;

namespace NetCore.Tools.Migration.DbContexts.Payroll
{
    public class PayrollDbContext : DbContext
    {
        public PayrollDbContext(DbContextOptions<PayrollDbContext> options) : base(options)
        {

        }

        public DbSet<EmployeeEntity> Employees { get; set; }
        public DbSet<SalaryEntity> Salaries { get; set; }
        public DbSet<CompulsoryContributionEntity> CompulsoryContributions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EmployeeEntity>(entity =>
            {
                entity.ToTable("Employee");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.EmpCode).HasColumnName("EmpCode");
            });

            const string SalarySeq = nameof(SalarySeq);

            modelBuilder.Entity<SalaryEntity>(entity =>
            {
                entity.ToTable("Salary");
                entity.Property(e => e.Id).UseHiLo(SalarySeq);

                entity.Property(e => e.EncryptedGrossSalary).HasColumnName("EncryptedGrossSalary");
                entity.Property(e => e.EncryptedNetSalary).HasColumnName("EncryptedNetSalary");
                entity.Property(e => e.EncryptedBaseSalary).HasColumnName("EncryptedBaseSalary");
                entity.Property(e => e.CcFullCover).HasColumnName("CcFullCover");
                entity.Property(e => e.ApplyDate).HasColumnName("ApplyDate");
                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeId");
                entity.Property(e => e.IsDeleted).HasColumnName("IsDeleted");
                entity.Property(e => e.CreatedTime).HasColumnName("CreatedTime");
            });

            const string CompulsoryContributionSeq = nameof(CompulsoryContributionSeq);

            modelBuilder.Entity<CompulsoryContributionEntity>(entity =>
            {
                entity.ToTable("CompulsoryContribution");
                entity.Property(o => o.Id).UseHiLo(CompulsoryContributionSeq);

                entity.Property(e => e.EmployeeRate).HasColumnName("EmployeeRate");
                entity.Property(e => e.EmployerRate).HasColumnName("EmployerRate");
                entity.Property(e => e.EmployeeSalaryCapInVnd).HasColumnName("EmployeeSalaryCapInVnd");
                entity.Property(e => e.EmployerSalaryCapInVnd).HasColumnName("EmployerSalaryCapInVnd");
                entity.Property(e => e.ApplyDate).HasColumnName("ApplyDate");
                entity.Property(e => e.CcType).HasColumnName("CcType");
            });
        }
    }
}
