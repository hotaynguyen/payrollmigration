﻿namespace NetCore.Tools.Migration.DbContexts.Payroll
{
    public class EmployeeEntity
    {
        public int Id { get; set; }
        public string EmpCode { get; set; }
    }
}
