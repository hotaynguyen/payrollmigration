﻿using System;

namespace EmpInfo_Migration.DbContexts.EmpInfo.EmpInfoEntities
{
    public class ContractEntity
    {
        public int Id { get; set; }
        public DateTime ChangeContractDate { get; set; }
        public bool IsSalaryChange { get; set; }
        public string NetSalary { get; set; }
        public string GrossSalary { get; set; }
        public string BaseSalary { get; set; }
        public double? SocialEnsurancePercentage { get; set; }
        public int EmployeeId { get; set; }
        public bool IsDeleted { get; set; }
        public virtual EmployeeEntity Employee { get; set; }
    }
}
