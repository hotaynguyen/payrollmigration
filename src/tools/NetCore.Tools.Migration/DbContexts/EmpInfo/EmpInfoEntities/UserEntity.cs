﻿namespace EmpInfo_Migration.DbContexts.EmpInfo.EmpInfoEntities
{
    public class UserEntity
    {
        public int Id { get; set; }
        public string EmpCode { get; set; }
    }
}
